import os
import random
import shutil
from tkinter import ttk
import tkinter as tk
from PIL import Image, ImageTk
import tkinter.font as font
import cv2
from tkinter import filedialog as fd
from tkinter.messagebox import showinfo
from tkPDFViewer import tkPDFViewer as pdf
import sqlite3
from tkinter import messagebox as msg
from fpdf import FPDF


class Object_Detector:

    def iesire(self):
        root.destroy()

    def selectare_linie(self, event=""):
        control_obiect = self.tabel_cautare.focus()
        continut = self.tabel_cautare.item(control_obiect)
        linie = continut["values"]
        if linie != "":
            self.cod.set(linie[0])
            self.nume.set(linie[1])
            self.pret.set(linie[2])
            self.nr_buc.set(linie[3])
            director_curent = os.getcwd()
            director_imagini = "Imagini obiecte"
            cale_abs = os.path.join(director_curent, director_imagini)
            os.chdir(cale_abs)
            if os.path.exists(linie[1]):
                fis = random.choice(os.listdir(os.path.join(os.getcwd() + "/" + linie[1])))
                os.chdir(os.path.join(os.getcwd() + "/" + linie[1]))
                img_obiect = Image.open(fis, mode='r')
                img_obiect = img_obiect.resize((400, 200), Image.Resampling.LANCZOS)
                self.obiect_imag = ImageTk.PhotoImage(img_obiect)
                poza_obiect = tk.Label(self.fereastra_copil, image=self.obiect_imag)
                poza_obiect.place(x=900, y=490, width=400, height=200)
                os.chdir(director_curent)
            else:
                os.chdir(director_curent)
                img_obiect = Image.open(r"no_image.png")
                img_obiect = img_obiect.resize((400, 200), Image.Resampling.LANCZOS)
                self.obiect_imag = ImageTk.PhotoImage(img_obiect)
                poza_obiect = tk.Label(self.fereastra_copil, image=self.obiect_imag)
                poza_obiect.place(x=900, y=490, width=400, height=200)

    def selectare_linie_cos(self, event=""):
        control_obiect = self.tabel_cos.focus()
        continut = self.tabel_cos.item(control_obiect)
        linie = continut["values"]
        if linie != "":
            self.cod.set(linie[0])
            self.nume.set(linie[1])
            self.pret.set(linie[2])
            self.nr_buc.set(linie[3])

    def save_manual(self):
        if self.cod1.get() != 0 and (self.nume1.get() == " " or self.pret1.get() == 0.0 or self.nr_buc1.get() == 0):
            msg.showinfo("Eroare la introducerea manuala a datelor ", "Date incomplete !", parent=self.fereastra_copil)
        else:
            v1 = self.cod1.get()
            v2 = self.nume1.get()
            v3 = self.pret1.get()
            v4 = self.nr_buc1.get()
            conector = sqlite3.connect('magazin.db')
            cursor1 = conector.cursor()
            cursor1.execute('insert into produs (cod_id, nume, pret, nr_buc) values (?,?,?,?)', (v1, v2, v3, v4))
            conector.commit()
            conector.close()
            self.vizualizare_obiecte_manual()
            self.vizualizare_obiecte()

            self.cod1.set(random.randint(1000, 9999))
            self.nume1.set("")
            self.pret1.set("")
            self.nr_buc1.set("")

    def inchide_fereastra_manuala(self):
        self.fereastra_copil1.destroy()

    def vizualizare_obiecte_manual(self):
        conector = sqlite3.connect('magazin.db')
        cursor1 = conector.cursor()
        cursor1.execute('select * from produs')
        date = cursor1.fetchall()
        # print(date)
        if date != 0:
            self.tabel_cautare_manual.delete(*self.tabel_cautare_manual.get_children())
            for linie in date:
                self.tabel_cautare_manual.insert("", tk.END, values=linie)
        conector.commit()
        conector.close()

    def adaugare_manuala(self):
        self.cod1 = tk.StringVar()
        self.nume1 = tk.StringVar()
        self.pret1 = tk.StringVar()
        self.nr_buc1 = tk.StringVar()
        self.fereastra_copil1 = tk.Toplevel(self.fereastra_copil)
        self.fereastra_copil1.geometry("1400x800+300+100")
        self.fereastra_copil1.title("Introducerea datelor pentru obiecte recunoscute")
        self.fereastra_copil1.resizable(False, False)
        self.cod1.set(random.randint(1000, 9999))

        img1 = Image.open(r"poza1.jpg")
        img1 = img1.resize((700, 200), Image.Resampling.LANCZOS)
        self.photoimg11 = ImageTk.PhotoImage(img1)
        poza_1 = tk.Label(self.fereastra_copil1, image=self.photoimg11)
        poza_1.place(x=0, y=0, width=700, height=200)

        img2 = Image.open(r"poza2.jpg")
        img2 = img2.resize((700, 200), Image.Resampling.LANCZOS)
        self.photoimg2 = ImageTk.PhotoImage(img2)
        poza2 = tk.Label(self.fereastra_copil1, image=self.photoimg2)
        poza2.place(x=700, y=0, width=700, height=200)

        img_fundal_antet = Image.open(r"poza3.png")
        img_fundal_antet = img_fundal_antet.resize((1400, 700), Image.Resampling.LANCZOS)
        self.photoimg_fundal_antet = ImageTk.PhotoImage(img_fundal_antet)

        poza_fundal_antet = tk.Label(self.fereastra_copil1, image=self.photoimg_fundal_antet)
        poza_fundal_antet.place(x=0, y=200, width=1400, height=700)

        titlu_antet = tk.Label(poza_fundal_antet, text="Adaugarea manuala a unui produs in depozit",
                               font=("times new roman", 30, "bold"), bg="#87CEFA", fg="white")
        titlu_antet.place(x=0, y=0, width=1400, height=40)

        frame1 = tk.Frame(poza_fundal_antet, bd=2, bg="white")
        frame1.place(x=10, y=50, width=1380, height=540)

        partea_stanga = tk.LabelFrame(frame1, bd=2, text="DATE PRODUS", font=("times new roman", 15, "bold"),
                                      bg="white")
        partea_stanga.place(x=10, y=10, width=620, height=470)

        label_cod = tk.Label(partea_stanga, text="Cod", font=("times new roman", 20, "bold"), bg="white",
                             fg="black").grid(row=0, column=0)
        text_cod = tk.Entry(partea_stanga, state="readonly", font=("times new roman", 20, "bold"), bg="white",
                            fg="black", width=35, textvariable=self.cod1).grid(row=0, column=1)

        label_nume = tk.Label(partea_stanga, text="Nume", font=("times new roman", 20, "bold"), bg="white",
                              fg="black").grid(row=1, column=0)
        text_nume = tk.Entry(partea_stanga, font=("times new roman", 20, "bold"), bg="white",
                             fg="black", width=35, textvariable=self.nume1).grid(row=1, column=1)

        label_pret = tk.Label(partea_stanga, text="Pret", font=("times new roman", 20, "bold"), bg="white",
                              fg="black").grid(row=2, column=0)
        text_pret = tk.Entry(partea_stanga, font=("times new roman", 20, "bold"), bg="white", fg="black", width=35,
                             textvariable=self.pret1).grid(row=2, column=1)

        label_nr_buc = tk.Label(partea_stanga, text="Nr buc", font=("times new roman", 20, "bold"), bg="white",
                                fg="black").grid(row=3, column=0)
        text_nr_buc = tk.Entry(partea_stanga, font=("times new roman", 20, "bold"), bg="white", fg="black", width=35,
                               textvariable=self.nr_buc1).grid(row=3, column=1)

        poza = tk.LabelFrame(self.fereastra_copil1, bd=2, text="IMAGINE PRODUS", font=("times new roman", 15, "bold"),
                             bg="white")
        poza.place(x=80, y=450, width=500, height=270)

        img_obiect = Image.open(r"no_image.png")
        img_obiect = img_obiect.resize((400, 200), Image.Resampling.LANCZOS)
        self.obiect_imag1 = ImageTk.PhotoImage(img_obiect)
        poza_obiect = tk.Label(self.fereastra_copil1, image=self.obiect_imag1)
        poza_obiect.place(x=100, y=490, width=400, height=200)

        but_save = tk.Button(frame1, text="Save", cursor="hand2", font=("times new roman", 15, "bold"), bg="#87CEFA",
                             fg="white", command=self.save_manual)
        but_save.place(x=270, y=485, width=300, height=40)

        but_inchidere = tk.Button(frame1, text="Cancel", cursor="hand2", font=("times new roman", 15, "bold"),
                                  bg="#87CEFA", fg="white", command=self.inchide_fereastra_manuala)
        but_inchidere.place(x=670, y=485, width=300, height=40)

        partea_dreapta = tk.LabelFrame(frame1, bd=2, text="LISTA PRODUSE", font=("times new roman", 15, "bold"),
                                       bg="white")
        partea_dreapta.place(x=650, y=10, width=670, height=470)
        scrol_x = ttk.Scrollbar(partea_dreapta, orient=tk.HORIZONTAL)
        scrol_y = ttk.Scrollbar(partea_dreapta, orient=tk.VERTICAL)
        self.tabel_cautare_manual = ttk.Treeview(partea_dreapta, column=("cod_id", "nume", "pret", "nr_buc"),
                                                 xscrollcommand=scrol_x.set, yscrollcommand=scrol_y)
        scrol_x.pack(side=tk.BOTTOM, fill=tk.X)
        scrol_y.pack(side=tk.RIGHT, fill=tk.Y)
        scrol_x.config(command=self.tabel_cautare_manual.xview)
        scrol_y.config(command=self.tabel_cautare_manual.yview)
        self.tabel_cautare_manual.heading("cod_id", text="Cod produs")
        self.tabel_cautare_manual.heading("nume", text="Nume produs")
        self.tabel_cautare_manual.heading("pret", text="Pret ")
        self.tabel_cautare_manual.heading("nr_buc", text="Numar bucati")
        self.vizualizare_obiecte_manual()
        self.tabel_cautare_manual["show"] = "headings"
        self.tabel_cautare_manual.pack(fill=tk.BOTH, expand=1)

    def adaugare_obiect(self):
        self.cod = tk.StringVar()
        self.nume = tk.StringVar()
        self.pret = tk.StringVar()
        self.nr_buc = tk.StringVar()
        self.fereastra_copil = tk.Toplevel(root)
        self.fereastra_copil.geometry("1400x800+300+100")
        self.fereastra_copil.title("Introducerea datelor pentru obiecte recunoscute")
        self.fereastra_copil.resizable(False, False)
        img_1_1 = Image.open(r"poza1.jpg")
        img_1_1 = img_1_1.resize((700, 200), Image.Resampling.LANCZOS)
        self.photoimg_1_1 = ImageTk.PhotoImage(img_1_1)
        poza_1_1_antet_1 = tk.Label(self.fereastra_copil, image=self.photoimg_1_1)
        poza_1_1_antet_1.place(x=0, y=0, width=700, height=200)

        img_1_2 = Image.open(r"poza2.jpg")
        img_1_2 = img_1_2.resize((700, 200), Image.Resampling.LANCZOS)
        self.photoimg_1_2 = ImageTk.PhotoImage(img_1_2)
        poza_1_2_antet_1 = tk.Label(self.fereastra_copil, image=self.photoimg_1_2)
        poza_1_2_antet_1.place(x=700, y=0, width=700, height=200)

        img_fundal_antet = Image.open(r"poza3.png")
        img_fundal_antet = img_fundal_antet.resize((1400, 700), Image.Resampling.LANCZOS)
        self.photoimg_fundal_antet = ImageTk.PhotoImage(img_fundal_antet)

        poza_fundal_antet = tk.Label(self.fereastra_copil, image=self.photoimg_fundal_antet)
        poza_fundal_antet.place(x=0, y=200, width=1400, height=700)

        titlu_antet = tk.Label(poza_fundal_antet, text="Adaugarea unui produs in depozit",
                               font=("times new roman", 30, "bold"), bg="#87CEFA", fg="white")
        titlu_antet.place(x=0, y=0, width=1400, height=40)

        frame1 = tk.Frame(poza_fundal_antet, bd=2, bg="white")
        frame1.place(x=10, y=50, width=1380, height=540)

        partea_stanga = tk.LabelFrame(frame1, bd=2, text="LISTA PRODUSE", font=("times new roman", 15, "bold"),
                                      bg="white")
        partea_stanga.place(x=10, y=10, width=760, height=470)
        scrol_x = ttk.Scrollbar(partea_stanga, orient=tk.HORIZONTAL)
        scrol_y = ttk.Scrollbar(partea_stanga, orient=tk.VERTICAL)
        self.tabel_cautare = ttk.Treeview(partea_stanga, column=("cod_id", "nume", "pret", "nr_buc"),
                                          xscrollcommand=scrol_x.set, yscrollcommand=scrol_y)
        scrol_x.pack(side=tk.BOTTOM, fill=tk.X)
        scrol_y.pack(side=tk.RIGHT, fill=tk.Y)
        scrol_x.config(command=self.tabel_cautare.xview)
        scrol_y.config(command=self.tabel_cautare.yview)
        self.tabel_cautare.heading("cod_id", text="Cod produs")
        self.tabel_cautare.heading("nume", text="Nume produs")
        self.tabel_cautare.heading("pret", text="Pret ")
        self.tabel_cautare.heading("nr_buc", text="Numar bucati")
        self.tabel_cautare.bind("<ButtonRelease>", self.selectare_linie)
        self.vizualizare_obiecte()
        self.tabel_cautare["show"] = "headings"
        self.tabel_cautare.pack(fill=tk.BOTH, expand=1)

        but_camera = tk.Button(frame1, text="Camera", cursor="hand2", font=("times new roman", 15, "bold"),
                               bg="#87CEFA",
                               fg="white", command=self.pornire_camera)
        but_camera.place(x=70, y=485, width=170, height=40)

        but_manual = tk.Button(frame1, text="Manual", cursor="hand2", font=("times new roman", 15, "bold"),
                               bg="#87CEFA",
                               fg="white", command=self.adaugare_manuala)
        but_manual.place(x=270, y=485, width=170, height=40)

        but_inchidere = tk.Button(frame1, text="Cancel", cursor="hand2", font=("times new roman", 15, "bold"),
                                  bg="#87CEFA", fg="white", command=lambda: self.fereastra_copil.destroy())
        but_inchidere.place(x=470, y=485, width=170, height=40)

        partea_dreapta = tk.LabelFrame(frame1, bd=2, text="DATE PRODUS", font=("times new roman", 15, "bold"),
                                       bg="white")
        partea_dreapta.place(x=780, y=10, width=590, height=470)

        label_cod = tk.Label(partea_dreapta, text="Cod", font=("times new roman", 20, "bold"), bg="white",
                             fg="black").grid(row=0, column=0)
        text_cod = tk.Entry(partea_dreapta, state="readonly", font=("times new roman", 20, "bold"), bg="white",
                            fg="black", width=35, textvariable=self.cod).grid(row=0, column=1)

        label_nume = tk.Label(partea_dreapta, text="Nume", font=("times new roman", 20, "bold"), bg="white",
                              fg="black").grid(row=1, column=0)
        text_nume = tk.Entry(partea_dreapta, state="readonly", font=("times new roman", 20, "bold"), bg="white",
                             fg="black", width=35,
                             textvariable=self.nume).grid(row=1, column=1)

        label_pret = tk.Label(partea_dreapta, text="Pret", font=("times new roman", 20, "bold"), bg="white",
                              fg="black").grid(row=2, column=0)
        text_pret = tk.Entry(partea_dreapta, font=("times new roman", 20, "bold"), bg="white", fg="black", width=35,
                             textvariable=self.pret).grid(row=2, column=1)

        label_nr_buc = tk.Label(partea_dreapta, text="Nr buc", font=("times new roman", 20, "bold"), bg="white",
                                fg="black").grid(row=3, column=0)
        text_nr_buc = tk.Entry(partea_dreapta, font=("times new roman", 20, "bold"), bg="white", fg="black", width=35,
                               textvariable=self.nr_buc).grid(row=3, column=1)

        poza = tk.LabelFrame(self.fereastra_copil, bd=2, text="IMAGINE PRODUS", font=("times new roman", 15, "bold"),
                             bg="white")
        poza.place(x=810, y=450, width=560, height=270)

        img_obiect = Image.open(r"no_image.png")
        img_obiect = img_obiect.resize((400, 200), Image.Resampling.LANCZOS)
        self.obiect_imag = ImageTk.PhotoImage(img_obiect)
        poza_obiect = tk.Label(self.fereastra_copil, image=self.obiect_imag)
        poza_obiect.place(x=900, y=490, width=400, height=200)

        but_update = tk.Button(frame1, text="Salveaza modificarile", cursor="hand2",
                               font=("times new roman", 15, "bold"), bg="#87CEFA",
                               fg="white", command=self.update_date)
        but_update.place(x=830, y=485, width=200, height=40)

        but_stergere = tk.Button(frame1, text="Stergere", cursor="hand2", font=("times new roman", 15, "bold"),
                                 bg="#87CEFA",
                                 fg="white", command=self.stergere)
        but_stergere.place(x=1150, y=485, width=150, height=40)

    def update_date(self):
        if self.nume.get() == "" or self.pret.get() == "" or self.nr_buc.get() == "":
            msg.showinfo("Eroare la modificarea datelor ", "Trebuie sa introduceti toate datele produsului !",
                         parent=self.fereastra_copil)
        else:
            conector = sqlite3.connect('magazin.db')
            cursor1 = conector.cursor()
            cursor1.execute("""UPDATE produs SET nume=?, pret=?, nr_buc=? WHERE cod_id=?""",
                            (self.nume.get(), self.pret.get(), self.nr_buc.get(), self.cod.get()))
            conector.commit()
            conector.close()
            # msg.showinfo("Succes !", "Modificarea a fost efectuata cu succes !", parent=self.root)
            self.vizualizare_obiecte()

    def update_date_cos(self):
        if self.nume.get() == "" or self.pret.get() == "" or self.nr_buc.get() == "":
            msg.showinfo("Eroare la modificarea datelor ", "Trebuie sa introduceti toate datele produsului !",
                         parent=self.cos)
        else:
            if int(self.nr_buc.get()) < 0:
                msg.showinfo("Eroare  !", "Cantitatea nu poate fi negativa !", parent=self.cos)
            elif int(self.nr_buc.get()) == 0:
                self.scoate_cos()
                self.vizualizare_cos()
                self.total_plata.set(self.determina_pret_cos())
            else:
                conector = sqlite3.connect('client.db')
                cursor1 = conector.cursor()
                cursor1.execute("""UPDATE cos SET nume=?, pret=?, nr_buc=? WHERE cod_id=?""",
                                (self.nume.get(), self.pret.get(), self.nr_buc.get(), self.cod.get()))
                conector.commit()
                conector.close()
                # msg.showinfo("Succes !", "Modificarea a fost efectuata cu succes !", parent=self.root)
                self.vizualizare_cos()
                self.total_plata.set(self.determina_pret_cos())

    def stergere(self):
        if self.cod.get() == "":
            msg.showinfo("Eroare stergere!", "Trebuie selectat un produs din lista!", parent=self.fereastra_copil)
            return
        optiune = msg.askyesno("Stergere produs!", "Sunteti sigur ca doriti eliminarea produsului ?",
                               parent=self.fereastra_copil)
        if optiune > 0:
            conector = sqlite3.connect('magazin.db')
            cursor1 = conector.cursor()
            comanda_stergere = "delete from produs where cod_id=?"
            valoare = (self.cod.get(),)
            cursor1.execute(comanda_stergere, valoare)
            conector.commit()
            conector.close()
            self.vizualizare_obiecte()
            self.resetare_formular()
            msg.showinfo("Eliminare produs !", "Stergerea a fost efectuata cu succes !", parent=self.fereastra_copil)

        else:
            return
        # self.vizualizare_obiecte()

    def resetare_formular(self):
        self.cod.set("")
        self.nume.set("")
        self.pret.set("")
        self.nr_buc.set("")

    def pornire_camera(self):
        cap = cv2.VideoCapture(0)  # 2 pentru camera externa
        cap.set(3, 640)
        cap.set(4, 480)

        nume_clasa = []
        fisier_clasa = "labels.txt"
        with open(fisier_clasa, "rt") as file:
            nume_clasa = file.read().rstrip('\n').split('\n')

        configPath = 'ssd_mobilenet_v3_large_coco_2020_01_14.pbtxt'
        wightPath = 'frozen_inference_graph.pb'

        net = cv2.dnn_DetectionModel(wightPath, configPath)

        net.setInputSize(320, 320)
        net.setInputScale(1.0 / 127.5)
        net.setInputMean((127.5, 127.5, 127.5))
        net.setInputSwapRB(True)
        lista_obiecte = {}
        dict_poze = {}
        import random
        try:
            while True:
                succes, img = cap.read()
                classIds, confs, bbox = net.detect(img, confThreshold=0.65)
                if (len(classIds) != 0 or len(bbox) != 0):
                    for classId, confidence, box in zip(classIds.flatten(), confs.flatten(), bbox):
                        cv2.rectangle(img, box, color=(255, 255, 0), thickness=2)
                        cv2.putText(img, nume_clasa[classId - 1].upper(), (box[0] + 10, box[1] + 30),
                                    cv2.FONT_HERSHEY_COMPLEX, 1, (0, 255, 0), 2)
                        cv2.putText(img, str(round(confidence * 100, 2)).upper(), (box[0] + 200, box[1] + 30),
                                    cv2.FONT_HERSHEY_COMPLEX, 1, (0, 255, 0), 2)
                        if nume_clasa[classId - 1] != 'persoana':
                            cod_produs = random.randint(1000, 9999)
                            lista_obiecte[nume_clasa[classId - 1]] = cod_produs  # round(confidence*100,2)
                cv2.imshow("Scanare obiect", img)
                if cv2.waitKey(1) == 27:
                    conector = sqlite3.connect('magazin.db')
                    cursor1 = conector.cursor()
                    for l1, l2 in lista_obiecte.items():
                        cursor1.execute('insert into produs (cod_id, nume, pret, nr_buc) values (?,?,?,?)',
                                        (l2, l1, 0, 0))

                    conector.commit()
                    conector.close()
                    self.vizualizare_obiecte()
                    # self.pune_imagine(l1, l2)
                    break
            cv2.destroyAllWindows()
            cap.release()
        except:
            cap.release()
            cv2.destroyAllWindows()

    def vizualizare_obiecte(self):
        conector = sqlite3.connect('magazin.db')
        cursor1 = conector.cursor()
        cursor1.execute('select * from produs')
        date = cursor1.fetchall()
        # print(date)
        if date != 0:
            self.tabel_cautare.delete(*self.tabel_cautare.get_children())
            for linie in date:
                self.tabel_cautare.insert("", tk.END, values=linie)
        conector.commit()
        conector.close()

    def vizualizare_cos(self):
        conector1 = sqlite3.connect('client.db')
        cursor2 = conector1.cursor()
        cursor2.execute('select * from cos')
        date = cursor2.fetchall()
        if date != 0:
            self.tabel_cos.delete(*self.tabel_cos.get_children())
            for linie in date:
                self.tabel_cos.insert("", tk.END, values=linie)
        conector1.commit()
        conector1.close()

    def determina_pret_cos(self):
        pret = 0.0
        conector1 = sqlite3.connect('client.db')
        cursor2 = conector1.cursor()
        cursor2.execute('select * from cos')
        date = cursor2.fetchall()
        if date != 0:
            for linie in date:
                pret = pret + linie[2] * linie[3]
        conector1.commit()
        conector1.close()
        return pret

    def cautare_duplicat(self, identificator):
        conector1 = sqlite3.connect('client.db')
        cursor2 = conector1.cursor()
        cursor2.execute("SELECT * from cos WHERE cod_id=?", (identificator,))
        date = cursor2.fetchall()
        if date == []:
            nr_bucati = 0
        else:
            nr_bucati = date[0][3]
        conector1.close()
        return nr_bucati

    def determina_cantitatea(self, identificator):
        conector1 = sqlite3.connect('magazin.db')
        cursor2 = conector1.cursor()
        cursor2.execute("SELECT * from produs WHERE cod_id=?", (identificator,))
        date = cursor2.fetchall()
        if date == []:
            nr_bucati = 0
        else:
            nr_bucati = date[0][3]
        conector1.close()
        return nr_bucati

    def alege_obiect(self, event=""):
        obiect = self.tabel_cautare.focus()
        continut_obiect = self.tabel_cautare.item(obiect)
        linie_selectata = continut_obiect["values"]
        if linie_selectata != "":
            valoare1 = linie_selectata[0]
            valoare2 = linie_selectata[1]
            valoare3 = linie_selectata[2]

            director_curent = os.getcwd()
            director_imagini = "Imagini obiecte"
            cale_abs = os.path.join(director_curent, director_imagini)
            os.chdir(cale_abs)
            if os.path.exists(valoare2):
                fis = random.choice(os.listdir(os.path.join(os.getcwd() + "/" + valoare2)))
                os.chdir(os.path.join(os.getcwd() + "/" + valoare2))

                img_obiect = Image.open(fis, mode='r')
                img_obiect = img_obiect.resize((400, 400), Image.Resampling.LANCZOS)
                self.obiect_imag = ImageTk.PhotoImage(img_obiect)
                poza_obiect = tk.Label(self.fereastra_copil, image=self.obiect_imag)
                poza_obiect.place(x=853, y=303, width=400, height=400)

                os.chdir(director_curent)
            else:
                os.chdir(director_curent)

                img_obiect = Image.open(r"no_image.png")
                img_obiect = img_obiect.resize((400, 400), Image.Resampling.LANCZOS)
                self.obiect_imag = ImageTk.PhotoImage(img_obiect)
                poza_obiect = tk.Label(self.fereastra_copil, image=self.obiect_imag)
                poza_obiect.place(x=853, y=303, width=400, height=400)

            return [valoare1, valoare2, valoare3]
        else:
            return []

    def scoate_obiect(self, event=""):
        obiect = self.tabel_cos.focus()
        continut_obiect = self.tabel_cos.item(obiect)
        linie_selectata = continut_obiect["values"]
        if linie_selectata != "":
            valoare1 = linie_selectata[0]
            valoare2 = linie_selectata[1]
            valoare3 = linie_selectata[2]
            return [valoare1, valoare2, valoare3]
        else:
            return []

    def vizualizare_factura(self, nume_fis="plata.pdf"):
        from tkPDFViewer import tkPDFViewer as pdf
        ob_factura = tk.Toplevel(self.cos)
        ob_factura.geometry("900x900+500+100")
        ob_factura.title("Factura electronica")
        ob_factura.resizable(False, False)

        but_inchidere = tk.Button(ob_factura, text="Inchidere", cursor="hand2", font=("times new roman", 15, "bold"),
                                  bg="#87CEFA", fg="white", command=lambda: ob_factura.destroy())
        but_inchidere.pack()
        nume_fis = open(nume_fis, mode='r')

        self.ob1_factura = pdf.ShowPdf()
        self.ob1_factura.img_object_li.clear()
        self.ob2_factura = self.ob1_factura.pdf_view(ob_factura, pdf_location=nume_fis, width=200, height=200)
        self.ob2_factura.pack(padx=(0, 0))

    def generare_factura(self):
        self.ob1_factura = None
        self.ob2_factura = None
        import datetime
        from random import randint
        data_curenta = datetime.datetime.now()
        fis_pdf = FPDF(orientation='P', unit='mm', format="A4")
        fis_pdf.add_page()
        fis_pdf.image(name="sigla.png", h=50, type="PNG")
        fis_pdf.set_font("Times", size=14)
        fis_pdf.set_text_color(0, 0, 255)
        # fis_pdf.set_title("Factura fisacala nr: "+str(randint(1000,9999)))
        with open("antet_factura.txt", "r") as fisier_txt:
            linii = fisier_txt.readlines()
        linie_pdf = ""
        x = 2
        for linie_pdf in linii:
            linie_pdf = linie_pdf.rstrip("\n")
            if linie_pdf.find("Client:") == 0:
                linie_pdf = linie_pdf + str(randint(1000, 9999))
            if linie_pdf.find("Data:") == 0:
                linie_pdf = linie_pdf + " " + str(data_curenta.day) + "/" + str(data_curenta.month) + "/" + str(
                    data_curenta.year)

            if linie_pdf.find("Factura seria F, nr:") == 0:
                linie_pdf = linie_pdf + str(randint(1000, 9999))
            if linie_pdf.find("Nume produs") == 0:
                fis_pdf.cell(200, 10, txt=linie_pdf, ln=x, align="L")
                fis_pdf.cell(200, 10, txt="-" * 80, ln=x + 10, align="L")

                pret_factura = 0.0
                conector1 = sqlite3.connect('client.db')
                cursor2 = conector1.cursor()
                cursor2.execute('select * from cos')
                date = cursor2.fetchall()

                if date != 0:
                    for linie in date:
                        pret_factura = pret_factura + linie[2] * linie[3]
                        linie_pdf = str(linie[1]) + " " * 20 + str(linie[0]) + " " * 20 + str(
                            linie[2]) + " " * 20 + str(linie[3])
                        fis_pdf.cell(200, 5, txt=linie_pdf, ln=x, align="L")
                        x += 1
                linie_pdf = ""
                conector1.commit()
                conector1.close()

            if linie_pdf.find('Pret fara TVA') == 0:
                linie_pdf = linie_pdf + " = " + str(pret_factura)
            if linie_pdf.find("Valoare TVA") == 0:
                linie_pdf = linie_pdf + " = " + str(pret_factura * 20 / 100)
            if linie_pdf.find("Total plata") == 0:
                linie_pdf = linie_pdf + "=" + str(pret_factura + pret_factura * 20 / 100)

            fis_pdf.cell(200, 6, txt=linie_pdf, ln=x, align="L")
            x = x + 1
        fis_pdf.cell(200, 6, txt="-" * 80, ln=x + 10, align="L")
        fis_pdf.output("plata.pdf")
        self.vizualizare_factura("plata.pdf")

    def adauga_cos(self):
        lista_valori = self.alege_obiect("<ButtonRelease>")
        if lista_valori != []:
            # aici cautam duplicatul !!!1
            nr_buc = self.cautare_duplicat(lista_valori[0])
            conector1 = sqlite3.connect('client.db')
            cursor2 = conector1.cursor()
            if nr_buc == 0:
                cursor2.execute('insert into cos (cod_id, nume, pret, nr_buc) values (?,?,?,?)',
                                (lista_valori[0], lista_valori[1], lista_valori[2], 1))
            else:
                nr_initial_bucati = self.determina_cantitatea(lista_valori[0])
                if nr_buc >= nr_initial_bucati:
                    msg.showinfo("Eroare !", "Numarul de obiecte comandate este mai mai decat ce este in stoc !",
                                 parent=self.fereastra_copil)
                else:
                    cursor2.execute("""UPDATE cos SET nume=?, pret=?, nr_buc=? WHERE cod_id=?""",
                                    (lista_valori[1], lista_valori[2], nr_buc + 1, lista_valori[0]))
            conector1.commit()
            conector1.close()
            # self.vizualizare_cos()
        else:
            msg.showinfo("Atentie !", f"Selecteaza produsul pentru adaugare in cos !", parent=self.fereastra_copil)

    def scoate_cos(self):
        lista_valori = self.scoate_obiect("<ButtonRelease>")
        if lista_valori != []:
            conector1 = sqlite3.connect('client.db')
            cursor2 = conector1.cursor()
            comanda = "delete from cos where cod_id=? AND nume=? AND pret=?"
            cursor2.execute(comanda, (lista_valori[0], lista_valori[1], lista_valori[2]))
            conector1.commit()
            conector1.close()
            self.vizualizare_cos()
            self.resetare_formular()
            self.total_plata.set(self.determina_pret_cos())
        else:
            msg.showinfo("Atentie !", f"Selecteaza obiectul din cos pentru eliminare!", parent=self.cos)

    def cos_cumparaturi(self):
        self.cod = tk.StringVar()
        self.nume = tk.StringVar()
        self.pret = tk.StringVar()
        self.nr_buc = tk.StringVar()
        self.total_plata = tk.StringVar()
        self.ob1_factura = None
        self.ob2_factura = None
        self.cos = tk.Toplevel(self.fereastra_copil)
        self.cos.geometry("1400x800+300+100")
        self.cos.title("Introducerea datelor pentru obiecte recunoscute")
        self.cos.resizable(False, False)
        img_1_1_cos = Image.open(r"poza1.jpg")
        img_1_1_cos = img_1_1_cos.resize((700, 200), Image.Resampling.LANCZOS)
        self.photoimg_1_1_cos = ImageTk.PhotoImage(img_1_1_cos)
        poza_1_1_antet_1_cos = tk.Label(self.cos, image=self.photoimg_1_1_cos)
        poza_1_1_antet_1_cos.place(x=0, y=0, width=700, height=200)

        img_1_2_cos = Image.open(r"poza2.jpg")
        img_1_2_cos = img_1_2_cos.resize((700, 200), Image.Resampling.LANCZOS)
        self.photoimg_1_2_cos = ImageTk.PhotoImage(img_1_2_cos)
        poza_1_2_antet_1_cos = tk.Label(self.cos, image=self.photoimg_1_2_cos)
        poza_1_2_antet_1_cos.place(x=700, y=0, width=700, height=200)

        img_fundal_antet_cos = Image.open(r"poza3.png")
        img_fundal_antet_cos = img_fundal_antet_cos.resize((1400, 700), Image.Resampling.LANCZOS)
        self.photoimg_fundal_antet_cos = ImageTk.PhotoImage(img_fundal_antet_cos)

        poza_fundal_antet_cos = tk.Label(self.cos, image=self.photoimg_fundal_antet_cos)
        poza_fundal_antet_cos.place(x=0, y=200, width=1400, height=700)

        titlu_antet_cos = tk.Label(poza_fundal_antet_cos, text="Cos cumparaturi",
                                   font=("times new roman", 30, "bold"), bg="#87CEFA", fg="white")
        titlu_antet_cos.place(x=0, y=0, width=1400, height=40)

        frame2 = tk.Frame(poza_fundal_antet_cos, bd=2, bg="white")
        frame2.place(x=10, y=50, width=1380, height=540)

        partea_stanga_cos = tk.LabelFrame(frame2, bd=2, text="PRODUSE AFLATE IN COS",
                                          font=("times new roman", 15, "bold"),
                                          bg="white")
        partea_stanga_cos.place(x=10, y=10, width=760, height=470)
        scrol_x = ttk.Scrollbar(partea_stanga_cos, orient=tk.HORIZONTAL)
        scrol_y = ttk.Scrollbar(partea_stanga_cos, orient=tk.VERTICAL)
        self.tabel_cos = ttk.Treeview(partea_stanga_cos, column=("cod_id", "nume", "pret", "nr_buc"),
                                      xscrollcommand=scrol_x.set, yscrollcommand=scrol_y)
        scrol_x.pack(side=tk.BOTTOM, fill=tk.X)
        scrol_y.pack(side=tk.RIGHT, fill=tk.Y)
        scrol_x.config(command=self.tabel_cos.xview)
        scrol_y.config(command=self.tabel_cos.yview)
        self.tabel_cos.heading("cod_id", text="Cod produs")
        self.tabel_cos.heading("nume", text="Nume produs")
        self.tabel_cos.heading("pret", text="Pret ")
        self.tabel_cos.heading("nr_buc", text="Numar bucati")
        self.tabel_cos.bind("<ButtonRelease>", self.selectare_linie_cos)
        self.vizualizare_cos()
        self.tabel_cos["show"] = "headings"
        self.tabel_cos.pack(fill=tk.BOTH, expand=1)
        self.total_plata.set(self.determina_pret_cos())
        partea_dreapta = tk.LabelFrame(frame2, bd=2, text="DATE PRODUS SELECTAT", font=("times new roman", 15, "bold"),
                                       bg="white")
        partea_dreapta.place(x=780, y=10, width=590, height=340)

        label_cod = tk.Label(partea_dreapta, text="Cod", font=("times new roman", 20, "bold"), bg="white",
                             fg="black").grid(row=0, column=0)
        text_cod = tk.Entry(partea_dreapta, state="readonly", font=("times new roman", 20, "bold"), bg="white",
                            fg="black", width=35, textvariable=self.cod).grid(row=0, column=1)

        label_nume = tk.Label(partea_dreapta, text="Nume", font=("times new roman", 20, "bold"), bg="white",
                              fg="black").grid(row=1, column=0)
        text_nume = tk.Entry(partea_dreapta, state="readonly", font=("times new roman", 20, "bold"), bg="white",
                             fg="black", width=35,
                             textvariable=self.nume).grid(row=1, column=1)

        label_pret = tk.Label(partea_dreapta, text="Pret", font=("times new roman", 20, "bold"), bg="white",
                              fg="black").grid(row=2, column=0)
        text_pret = tk.Entry(partea_dreapta, state="readonly", font=("times new roman", 20, "bold"), bg="white",
                             fg="black", width=35,
                             textvariable=self.pret).grid(row=2, column=1)

        label_nr_buc = tk.Label(partea_dreapta, text="Nr buc", font=("times new roman", 20, "bold"), bg="white",
                                fg="black").grid(row=3, column=0)
        text_nr_buc = tk.Entry(partea_dreapta, font=("times new roman", 20, "bold"), bg="white", fg="black", width=35,
                               textvariable=self.nr_buc).grid(row=3, column=1)

        but_update = tk.Button(frame2, text="Salveaza modificarile", cursor="hand2",
                               font=("times new roman", 15, "bold"), bg="#87CEFA",
                               fg="white", command=self.update_date_cos)
        but_update.place(x=840, y=205, width=200, height=40)

        but_factura = tk.Button(frame2, text="Generare factura", cursor="hand2", font=("times new roman", 13, "bold"),
                                bg="#87CEFA", fg="white", command=self.generare_factura)
        but_factura.place(x=1130, y=205, width=200, height=40)

        but_stergere = tk.Button(frame2, text="Stergere", cursor="hand2", font=("times new roman", 15, "bold"),
                                 bg="#87CEFA",
                                 fg="white", command=self.scoate_cos)
        but_stergere.place(x=840, y=265, width=200, height=40)

        but_inchidere = tk.Button(frame2, text="Inapoi", cursor="hand2", font=("times new roman", 13, "bold"),
                                  bg="#87CEFA", fg="white", command=lambda: self.cos.destroy())
        but_inchidere.place(x=1130, y=265, width=200, height=40)

        frame_total_plata = tk.LabelFrame(frame2, bd=2, text="Total plata obiecte cos fara TVA",
                                          font=("times new roman", 15, "bold"),
                                          bg="white")
        frame_total_plata.place(x=780, y=370, width=590, height=100)

        label_total_plata = tk.Label(frame_total_plata, text="Lei :      ", font=("times new roman", 20, "bold"),
                                     bg="white",
                                     fg="black").grid(row=1, column=0)
        text_plata = tk.Entry(frame_total_plata, state="readonly", font=("times new roman", 20, "bold"), bg="white",
                              fg="black", width=35,
                              textvariable=self.total_plata).grid(row=1, column=1)

    def shopping(self):
        self.ob1_factura = None
        self.ob2_factura = None

        self.fereastra_copil = tk.Toplevel(root)
        self.fereastra_copil.geometry("1400x800+300+100")
        self.fereastra_copil.title("Cumparare produse")
        self.fereastra_copil.resizable(False, False)
        img_1_1 = Image.open(r"poza1.jpg")
        img_1_1 = img_1_1.resize((700, 200), Image.Resampling.LANCZOS)
        self.photoimg_1_1 = ImageTk.PhotoImage(img_1_1)
        poza_1_1_antet_1 = tk.Label(self.fereastra_copil, image=self.photoimg_1_1)
        poza_1_1_antet_1.place(x=0, y=0, width=700, height=200)

        img_1_2 = Image.open(r"poza2.jpg")
        img_1_2 = img_1_2.resize((700, 200), Image.Resampling.LANCZOS)
        self.photoimg_1_2 = ImageTk.PhotoImage(img_1_2)
        poza_1_2_antet_1 = tk.Label(self.fereastra_copil, image=self.photoimg_1_2)
        poza_1_2_antet_1.place(x=700, y=0, width=700, height=200)

        img_fundal_antet = Image.open(r"poza3.png")
        img_fundal_antet = img_fundal_antet.resize((1400, 700), Image.Resampling.LANCZOS)
        self.photoimg_fundal_antet = ImageTk.PhotoImage(img_fundal_antet)

        poza_fundal_antet = tk.Label(self.fereastra_copil, image=self.photoimg_fundal_antet)
        poza_fundal_antet.place(x=0, y=200, width=1400, height=700)

        titlu_antet = tk.Label(poza_fundal_antet, text="Cumpărare produse",
                               font=("times new roman", 30, "bold"), bg="#87CEFA", fg="white")
        titlu_antet.place(x=0, y=0, width=1400, height=40)

        frame1 = tk.Frame(poza_fundal_antet, bd=2, bg="white")
        frame1.place(x=10, y=50, width=1380, height=540)

        partea_stanga = tk.LabelFrame(frame1, bd=2, text="LISTĂ PRODUSE", font=("times new roman", 15, "bold"),
                                      bg="white")
        partea_stanga.place(x=10, y=10, width=600, height=520)
        scrol_x = ttk.Scrollbar(partea_stanga, orient=tk.HORIZONTAL)
        scrol_y = ttk.Scrollbar(partea_stanga, orient=tk.VERTICAL)
        self.tabel_cautare = ttk.Treeview(partea_stanga, column=("cod_id", "nume", "pret", "nr_buc"),
                                          xscrollcommand=scrol_x.set, yscrollcommand=scrol_y)
        scrol_x.pack(side=tk.BOTTOM, fill=tk.X)
        scrol_y.pack(side=tk.RIGHT, fill=tk.Y)
        scrol_x.config(command=self.tabel_cautare.xview)
        scrol_y.config(command=self.tabel_cautare.yview)
        self.tabel_cautare.heading("cod_id", text="Cod produs")
        self.tabel_cautare.heading("nume", text="Nume produs")
        self.tabel_cautare.heading("pret", text="Pret ")
        self.tabel_cautare.heading("nr_buc", text="Nr bucati")
        self.tabel_cautare.bind("<ButtonRelease>", self.alege_obiect)
        self.vizualizare_obiecte()
        self.tabel_cautare["show"] = "headings"
        self.tabel_cautare.pack(fill=tk.BOTH, expand=1)

        img_adauga = Image.open(r"adauga.png")
        img_adauga = img_adauga.resize((100, 100), Image.Resampling.LANCZOS)
        self.buton_adauga = ImageTk.PhotoImage(img_adauga)
        but_adauga = tk.Button(frame1, image=self.buton_adauga, cursor="hand2", command=self.adauga_cos)
        but_adauga.place(x=620, y=200)

        poza = tk.LabelFrame(frame1, bd=2, text="IMAGINE PRODUS", font=("times new roman", 15, "bold"),
                             bg="white")
        poza.place(x=750, y=10, width=600, height=450)

        img_obiect = Image.open(r"no_image.png")
        img_obiect = img_obiect.resize((400, 400), Image.Resampling.LANCZOS)
        self.obiect_imag = ImageTk.PhotoImage(img_obiect)
        poza_obiect = tk.Label(frame1, image=self.obiect_imag)
        poza_obiect.place(x=840, y=50, width=400, height=370)
        #

        but_cos_cumparaturi = tk.Button(frame1, text="Cos cumparaturi", cursor="hand2",
                                        font=("times new roman", 13, "bold"),
                                        bg="#87CEFA",
                                        fg="white", command=self.cos_cumparaturi)
        but_cos_cumparaturi.place(x=800, y=485, width=150, height=40)

        but_inchidere = tk.Button(frame1, text="Inapoi", cursor="hand2", font=("times new roman", 13, "bold"),
                                  bg="#87CEFA", fg="white", command=lambda: self.fereastra_copil.destroy())
        but_inchidere.place(x=1140, y=485, width=150, height=40)

    def documentatie(self):
        # from tkPDFViewer import tkPDFViewer as pdf
        ob_help = tk.Toplevel()
        ob_help.geometry("900x900+500+100")
        ob_help.title("Documentatia aplicatiei")
        ob_help.resizable(False, False)

        but_inchidere = tk.Button(ob_help, text="Inchidere", cursor="hand2", font=("times new roman", 15, "bold"),
                                  bg="#87CEFA", fg="white", command=lambda: ob_help.destroy())
        but_inchidere.pack()

        self.ob1_pdf = pdf.ShowPdf()
        self.ob1_pdf.img_object_li.clear()
        self.ob2_pdf = self.ob1_pdf.pdf_view(ob_help, pdf_location=r"licenta2024.pdf", width=200, height=200)
        self.ob2_pdf.pack(padx=(0, 0))

    def verifica_utilizator(self):
        lista_personal = {'admin': '1234', 'biancabuzatu61@gmail.com': '5678'}
        if self.user_name.get() not in lista_personal or self.parola.get() != lista_personal[
            self.user_name.get()] or self.user_name.get() == "" or self.parola.get() == "":
            msg.showinfo("Eroare !", f"Utilizatorul nu exista inregistrat in aplicatie sau aveti date incomplete !",
                         parent=self.fereastra_logare)
            self.user_name.set("")
            self.parola.set("")
        else:

            if self.user_name.get() != "admin" and self.parola.get() == lista_personal[self.user_name.get()]:
                self.but_1["state"] = "disabled"
                self.but_1_1["state"] = "disabled"
            else:
                self.but_1["state"] = "normal"
                self.but_1_1["state"] = "normal"

            self.fereastra_logare.destroy()
            self.but_0_0["text"] = self.user_name.get()

    def logare(self):
        self.fereastra_logare = tk.Toplevel(root)
        self.fereastra_logare.geometry("700x500+600+250")
        self.fereastra_logare.title("Logare utilizator")
        self.fereastra_logare.resizable(False, False)

        self.fata_login = ImageTk.PhotoImage(file="fata_login.png")
        label_login = tk.Label(self.fereastra_logare, image=self.fata_login)
        label_login.grid(row=0, column=1)

        label_nula = tk.Label(self.fereastra_logare, text="", font=("times new roman", 20, "bold"),
                              fg="black").grid(row=1, column=0)

        label_user = tk.Label(self.fereastra_logare, text="Nume utilizator", font=("times new roman", 20, "bold"),
                              fg="black").grid(row=2, column=0)

        self.user_name = tk.StringVar()
        text_user = tk.Entry(self.fereastra_logare, font=("times new roman", 20, "bold"), bg="white", width=30,
                             textvariable=self.user_name).grid(row=2, column=1)

        label_nula = tk.Label(self.fereastra_logare, text="", font=("times new roman", 20, "bold"),
                              fg="black").grid(row=3, column=0)

        label_passwd = tk.Label(self.fereastra_logare, text="Parola", font=("times new roman", 20, "bold"),
                                fg="black").grid(row=4, column=0)

        self.parola = tk.StringVar()

        text_passwd = tk.Entry(self.fereastra_logare, font=("times new roman", 20, "bold"), bg="white", width=30,
                               textvariable=self.parola).grid(row=4, column=1)

        but_ok = tk.Button(self.fereastra_logare, text="Ok", cursor="hand2",
                           font=("times new roman", 13, "bold"),
                           bg="cyan", fg="white", command=self.verifica_utilizator)
        but_ok.place(x=100, y=360, width=150, height=40)

        but_close = tk.Button(self.fereastra_logare, text="Cancel", cursor="hand2",
                              font=("times new roman", 13, "bold"),
                              bg="cyan", fg="white", command=lambda: self.fereastra_logare.destroy())
        but_close.place(x=390, y=360, width=150, height=40)

    def __init__(self, root):
        self.root = root
        self.user_name = tk.StringVar()
        self.parola = tk.StringVar()

        latime = self.root.winfo_screenwidth()
        inaltime = self.root.winfo_screenheight()
        root.geometry("%dx%d" % (latime, inaltime))
        # print(latime, inaltime)
        self.root.title("Licenta - Buzatu Bianca Liliana - Detectarea obiectelor")

        self.fundal = ImageTk.PhotoImage(file="fundal2.png")
        label_fundal = tk.Label(self.root, image=self.fundal)
        label_fundal.place(x=5, y=5)

        ###

        img_b0 = Image.open(r"login.png")
        img_b0 = img_b0.resize((200, 120), Image.Resampling.LANCZOS)
        self.buton_logare = ImageTk.PhotoImage(img_b0)
        self.but_0 = tk.Button(self.root, image=self.buton_logare, cursor="hand2", command=self.logare)
        self.but_0.place(x=5, y=890)

        self.but_0_0 = tk.Button(self.root, text=self.user_name.get(), cursor="hand2", fg='white', bg="#87CEFA",
                                 width=28)
        self.but_0_0.place(x=5, y=1020)

        ###

        img_b1 = Image.open(r"video1.jpg")
        img_b1 = img_b1.resize((200, 120), Image.Resampling.LANCZOS)
        self.buton_online = ImageTk.PhotoImage(img_b1)
        self.but_1 = tk.Button(self.root, image=self.buton_online, cursor="hand2", command=self.adaugare_obiect)

        if self.user_name.get() != "admin":
            self.but_1["state"] = "disabled"
        else:
            self.but_1["state"] = "normal"

        self.but_1.place(x=405, y=890)

        self.but_1_1 = tk.Button(self.root, text="Adaugare produs", cursor="hand2", fg='white', bg="#87CEFA",
                                 command=self.adaugare_obiect, width=28)
        if self.user_name.get() != "admin":
            self.but_1_1["state"] = "disabled"
        else:
            self.but_1_1["state"] = "normal"
        self.but_1_1.place(x=405, y=1020)

        # buton 2

        img_b3 = Image.open(r"poza.png")
        img_b3 = img_b3.resize((200, 120), Image.Resampling.LANCZOS)
        self.buton_poza = ImageTk.PhotoImage(img_b3)
        but_3 = tk.Button(self.root, image=self.buton_poza, cursor="hand2", command=self.shopping)
        but_3.place(x=850, y=890)  # x=640
        but_3_3 = tk.Button(self.root, text="Cumparare produse", cursor="hand2", fg='white', bg="#87CEFA",
                            command=self.shopping, width=28)
        but_3_3.place(x=850, y=1020)

        # buton 3

        img_b4 = Image.open(r"help.png")
        img_b4 = img_b4.resize((200, 120), Image.Resampling.LANCZOS)
        self.buton_help = ImageTk.PhotoImage(img_b4)
        but_4 = tk.Button(self.root, image=self.buton_help, cursor="hand2", command=self.documentatie)
        but_4.place(x=1255, y=890)
        but_4_4 = tk.Button(self.root, text="Documentatie", cursor="hand2", fg='white', bg="#87CEFA",
                            command=self.documentatie, width=28)
        but_4_4.place(x=1255, y=1020)

        # buton 4
        img_b5 = Image.open(r"Exit.png")
        img_b5 = img_b5.resize((200, 120), Image.Resampling.LANCZOS)
        self.buton_exit = ImageTk.PhotoImage(img_b5)
        but_5 = tk.Button(self.root, image=self.buton_exit, cursor="hand2", command=self.iesire)
        but_5.place(x=1700, y=890)
        but_5_5 = tk.Button(self.root, text="Exit", cursor="hand2", fg='white', bg="#87CEFA", command=self.iesire,
                            width=28)
        but_5_5.place(x=1700, y=1020)

        root.resizable(False, False)


if __name__ == "__main__":
    root = tk.Tk()
    rad = Object_Detector(root)
    root.mainloop()

